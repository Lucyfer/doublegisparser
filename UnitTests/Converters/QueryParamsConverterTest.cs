﻿using System.Runtime.Serialization;
using DoubleGisParser.Converters;
using DoubleGisParser.Converters.Exceptions;
using DoubleGisParser.Converters.Impl;
using DoubleGisParser.Models.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.Converters
{
    [TestClass]
    public class QueryParamsConverterTest
    {
        private readonly IQueryParamsConverter _converter = new QueryParamsConverter();

        [TestMethod]
        public void TestToQueryParams_with_public_props()
        {
            var obj = new StubClassWithPublicProperties("string1", 2, "string2");
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("Prop1=string1&Prop2=2", result);
        }

        [TestMethod]
        public void TestToQueryParams_with_public_fields()
        {
            var obj = new StubClassWithPublicFields("string1", 2, "string2");
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("Field1=string1&Field2=2", result);
        }

        [TestMethod]
        public void TestToQueryParams_with_attributes()
        {
            var obj = new StubClassWithAttributes("string1", 2, "string2");
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("NewName=string1&Prop2=2", result);
        }

        [TestMethod]
        public void TestToQueryParams_with_attributes_empty_name()
        {
            var obj = new StubClassWithAttributesEmptyName("string1", "string2", "string3");
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("Field1=string1&Field2=string2&Field3=string3", result);
        }

        [TestMethod]
        public void TestQueryParams_anonymous_object()
        {
            var obj = new { Field1 = "field1", Field2 = 12.34 };
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("Field1=field1&Field2=12.34", result);
        }

        [TestMethod]
        [ExpectedException(typeof (RequiredParamsMissedException))]
        public void TestQueryParams_without_required_params()
        {
            var obj = new StubClassWithRequiredFields(null, null);
            _converter.ToQueryParams(obj);
        }

        [TestMethod]
        public void TestQueryParams_with_required_params()
        {
            var obj = new StubClassWithRequiredFields("test", 10);
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("Field1=test&Field2=10", result);
        }

        [TestMethod]
        public void TestQueryParams_rubricator_request_model()
        {
            var obj = new RubricatorRequestModel("test1", "test2");
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=test1&where=test2", result);
        }

        [TestMethod]
        [ExpectedException(typeof (RequiredParamsMissedException))]
        public void TestQueryParams_rubricator_request_without_required_param()
        {
            var obj = new RubricatorRequestModel("test1", null);
            _converter.ToQueryParams(obj);
        }

        [TestMethod]
        public void TestQueryParams_geo_request_model()
        {
            var obj = new GeoRequestModel("test1", "test2");
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=test1&q=test2", result);
        }

        [TestMethod]
        public void TestQueryParams_geo_request_model_full()
        {
            var obj = new GeoRequestModel("test1", "test2")
            {
                Format = "format",
                Limit = 1,
                Project = 2,
                Radius = 3,
                Types = "types"
            };
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=test1&q=test2&format=format&limit=1&project=2&radius=3&types=types", result);
        }

        [TestMethod]
        public void TestQueryParams_geo_request_bounds()
        {
            var obj = new GeoRequestModel("", "")
            {
                TopLeftBound = "34.2663,85.9824",
                BottomRightBound = "-62.737,73.156"
            };
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=&q=&bound[point2]=-62.737%2c73.156&bound[point1]=34.2663%2c85.9824", result);
        }

        [TestMethod]
        public void TestQueryParams_search_request_model()
        {
            var obj = new SearchRequestModel("key1", "what2", "where3");
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=key1&what=what2&where=where3", result);
        }

        [TestMethod]
        public void TestQueryParams_search_request_model_top_left()
        {
            var obj = new SearchRequestModel("", "", "")
            {
                TopLeftBound = "45.0012,84.54002"
            };
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=&what=&where=&bound[point1]=45.0012%2c84.54002", result);
        }

        [TestMethod]
        public void TestQueryParams_search_request_model_bottom_right()
        {
            var obj = new SearchRequestModel("", "", "")
            {
                BottomRightBound = "10.20035,73.985059"
            };
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=&what=&where=&bound[point2]=10.20035%2c73.985059", result);
        }

        [TestMethod]
        public void TestQueryParams_search_request_radius()
        {
            var obj = new SearchRequestModel("", "", "")
            {
                Radius = 197
            };
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=&what=&where=&radius=197", result);
        }

        [TestMethod]
        public void TestQueryParams_search_request_point()
        {
            var obj = new SearchRequestModel("", "", "")
            {
                Point = "36.00201,67.09807"
            };
            var result = _converter.ToQueryParams(obj);
            Assert.AreEqual("version=1.3&key=&what=&where=&point=36.00201%2c67.09807", result);
        }

        #region Stubs

        private class StubClassWithAttributes
        {
            public StubClassWithAttributes(string prop1, int prop2, string prop3)
            {
                Prop1 = prop1;
                Prop2 = prop2;
                Prop3 = prop3;
            }

            [DataMember(Name = "NewName")]
            public string Prop1 { get; set; }

            public int Prop2 { get; set; }

            [IgnoreDataMember]
            public string Prop3 { get; set; }
        }

        public class StubClassWithAttributesEmptyName
        {
            [DataMember]
            public string Field1;

            [DataMember(Name = "")]
            public string Field2;

            [DataMember(Name = null)]
            public string Field3;

            public StubClassWithAttributesEmptyName(string field1, string field2, string field3)
            {
                Field1 = field1;
                Field2 = field2;
                Field3 = field3;
            }
        }

        private class StubClassWithPublicFields
        {
            public string Field1 = "field1";
            public int Field2 = 2;

            private string ShoudlBeNotPresent;

            public StubClassWithPublicFields(string field1, int field2, string shoudlBeNotPresent)
            {
                Field1 = field1;
                Field2 = field2;
                ShoudlBeNotPresent = shoudlBeNotPresent;
            }
        }

        private class StubClassWithPublicProperties
        {
            public StubClassWithPublicProperties(string prop1, int prop2, string shoudlBeNotPresent)
            {
                Prop1 = prop1;
                Prop2 = prop2;
                ShoudlBeNotPresent = shoudlBeNotPresent;
            }

            public string Prop1 { get; set; }
            public int Prop2 { get; set; }

            private string ShoudlBeNotPresent { get; set; }
        }

        private class StubClassWithRequiredFields
        {
            [DataMember(IsRequired = true)]
            public string Field1;

            [DataMember(IsRequired = true)]
            public int? Field2;

            public StubClassWithRequiredFields(string field1, int? field2)
            {
                Field1 = field1;
                Field2 = field2;
            }
        }

        #endregion
    }
}
