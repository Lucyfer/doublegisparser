﻿using DoubleGisParser.Builders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.Builders
{
    [TestClass]
    public class GeoRequestBuilderTest
    {
        [TestMethod]
        public void TestBuilder_empty()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("someKey").WithPoint(1.1, 2.2);
            var request = builder.Build();
            Assert.AreEqual("someKey", request.Key);
            Assert.AreEqual("1.1,2.2", request.Query);
            Assert.IsNull(request.Format);
            Assert.IsNull(request.Limit);
            Assert.IsNull(request.Project);
            Assert.IsNull(request.Radius);
            Assert.IsNull(request.TopLeftBound);
            Assert.IsNull(request.BottomRightBound);
            Assert.IsNull(request.Types);
        }

        [TestMethod]
        public void TestBuilder_all()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("someKey")
                                           .WithCustomQuery("query")
                                           .SetFormat(GeoRequestBuilder.Format.Full)
                                           .SetLimit(100)
                                           .WithProject(10)
                                           .WithRadius(20)
                                           .WithTopLeftBound(3, 4)
                                           .WithBottomRightBound(5, 6)
                                           .WithTypes(GeoRequestBuilder.Types.City);
            var request = builder.Build();
            Assert.AreEqual("someKey", request.Key);
            Assert.AreEqual("query", request.Query);
            Assert.AreEqual("full", request.Format);
            Assert.AreEqual(100, request.Limit);
            Assert.AreEqual(10, request.Project);
            Assert.AreEqual(20, request.Radius);
            Assert.AreEqual("3,4", request.TopLeftBound);
            Assert.AreEqual("5,6", request.BottomRightBound);
            Assert.AreEqual("city", request.Types);
        }

        [TestMethod]
        public void TestBuildTypes()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("")
                                           .WithTypes(GeoRequestBuilder.Types.City, GeoRequestBuilder.Types.Crossroad,
                                                      GeoRequestBuilder.Types.District, GeoRequestBuilder.Types.House,
                                                      GeoRequestBuilder.Types.LivingArea, GeoRequestBuilder.Types.Metro,
                                                      GeoRequestBuilder.Types.Place, GeoRequestBuilder.Types.Project,
                                                      GeoRequestBuilder.Types.Settlement, GeoRequestBuilder.Types.Sight,
                                                      GeoRequestBuilder.Types.Station,
                                                      GeoRequestBuilder.Types.StationPlatform,
                                                      GeoRequestBuilder.Types.Street);
            var request = builder.Build();

            Assert.AreEqual("city,crossroad,district,house,living_area,metro,place,"
                            + "project,settlement,sight,station,station_platform,street", request.Types);
        }

        [TestMethod]
        public void TestBuildFullFormat()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("").SetFormat(GeoRequestBuilder.Format.Full);
            var response = builder.Build();
            Assert.AreEqual("full", response.Format);
        }

        [TestMethod]
        public void TestBuildShortFormat()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("").SetFormat(GeoRequestBuilder.Format.Short);
            var response = builder.Build();
            Assert.AreEqual("short", response.Format);
        }

        [TestMethod]
        public void TestBuildTopLeftBound()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("").WithTopLeftBound(10.0045, -49.29993);
            var response = builder.Build();
            Assert.AreEqual("10.0045,-49.29993", response.TopLeftBound);
        }

        [TestMethod]
        public void TestBuildBottomRightBound()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("").WithBottomRightBound(-72.7362, 63.0363);
            var response = builder.Build();
            Assert.AreEqual("-72.7362,63.0363", response.BottomRightBound);
        }

        [TestMethod]
        public void TestBuild_radius_negative()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("").WithRadius(-1);
            var response = builder.Build();
            Assert.IsNull(response.Radius);
        }

        [TestMethod]
        public void TestBuild_radius_zero()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("").WithRadius(0);
            var response = builder.Build();
            Assert.AreEqual(0, response.Radius);
        }

        [TestMethod]
        public void TestBuild_radius_max()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("").WithRadius(250);
            var response = builder.Build();
            Assert.AreEqual(250, response.Radius);
        }

        [TestMethod]
        public void TestBuild_radius_greater_than_max()
        {
            var builder = GeoRequestBuilder.CreateRequestWithKey("").WithRadius(251);
            var response = builder.Build();
            Assert.IsNull(response.Radius);
        }
    }
}
