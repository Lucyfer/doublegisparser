﻿using DoubleGisParser.Builders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.Builders
{
    [TestClass]
    public class SearchRequestBuilderTest
    {
        [TestMethod]
        public void TestBuilder_empty()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("someKey");
            var request = builder.Build();
            Assert.AreEqual("someKey", request.Key);
            Assert.IsNull(request.What);
            Assert.IsNull(request.Where);
            Assert.IsNull(request.Filters);
            Assert.IsNull(request.Page);
            Assert.IsNull(request.PageSize);
            Assert.IsNull(request.Sort);
        }

        [TestMethod]
        public void TestBuilder_all()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("someKey")
                                              .SearchFor("something")
                                              .WithWhere("where")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Mon, 09, 01)
                                              .SetPage(2)
                                              .SetPageSize(3)
                                              .SetSorting(SearchRequestBuilder.Sort.Rating);
            var request = builder.Build();
            Assert.AreEqual("someKey", request.Key);
            Assert.AreEqual("something", request.What);
            Assert.AreEqual("where", request.Where);
            Assert.AreEqual("filters[worktime]=mon,09:01", request.Filters);
            Assert.AreEqual(2, request.Page);
            Assert.AreEqual(3, request.PageSize);
            Assert.AreEqual("rating", request.Sort);
        }

        [TestMethod]
        public void TestBuilder_filter_normal()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Tue, 11, 35);
            var request = builder.Build();
            Assert.AreEqual("filters[worktime]=tue,11:35", request.Filters);
        }

        [TestMethod]
        public void TestBuilder_filter_incorrect_hour()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Wed, 90, 35);
            var request = builder.Build();
            Assert.IsNull(request.Filters);
        }

        [TestMethod]
        public void TestBuilder_filter_incorrect_hour_24()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Wed, 24, 00);
            var request = builder.Build();
            Assert.IsNull(request.Filters);
        }

        [TestMethod]
        public void TestBuilder_filter_zero_hour()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Wed, 00, 06);
            var request = builder.Build();
            Assert.AreEqual("filters[worktime]=wed,00:06", request.Filters);
        }

        [TestMethod]
        public void TestBuilder_filter_zero_minute()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Thu, 01, 00);
            var request = builder.Build();
            Assert.AreEqual("filters[worktime]=thu,01:00", request.Filters);
        }

        [TestMethod]
        public void TestBuilder_filter_59_minute()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Fri, 02, 59);
            var request = builder.Build();
            Assert.AreEqual("filters[worktime]=fri,02:59", request.Filters);
        }

        [TestMethod]
        public void TestBuilder_filter_negative_minute()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Sat, 02, -01);
            var request = builder.Build();
            Assert.IsNull(request.Filters);
        }

        [TestMethod]
        public void TestBuilder_filter_incorrect_minute()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Sun, 03, 60);
            var request = builder.Build();
            Assert.IsNull(request.Filters);
        }

        [TestMethod]
        public void TestBuilder_filter_alltime()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetWorktimeFilter(SearchRequestBuilder.Days.Sat);
            var request = builder.Build();
            Assert.AreEqual("filters[worktime]=sat,alltime", request.Filters);
        }

        [TestMethod]
        public void TestBuilder_sorting_relevance()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetSorting(SearchRequestBuilder.Sort.Relevance);
            var request = builder.Build();
            Assert.AreEqual("relevance", request.Sort);
        }

        [TestMethod]
        public void TestBuilder_sorting_distance()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetSorting(SearchRequestBuilder.Sort.Distance);
            var request = builder.Build();
            Assert.AreEqual("distance", request.Sort);
        }

        [TestMethod]
        public void TestBuilder_sorting_name()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("")
                                              .SetSorting(SearchRequestBuilder.Sort.Name);
            var request = builder.Build();
            Assert.AreEqual("name", request.Sort);
        }

        [TestMethod]
        public void TestBuilder_radius_min()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("").WithRadius(1);
            var request = builder.Build();
            Assert.AreEqual(1, request.Radius);
        }

        [TestMethod]
        public void TestBuilder_radius_less_than_min()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("").WithRadius(0);
            var request = builder.Build();
            Assert.IsNull(request.Radius);
        }

        [TestMethod]
        public void TestBuilder_radius_max()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("").WithRadius(40000);
            var request = builder.Build();
            Assert.AreEqual(40000, request.Radius);
        }

        [TestMethod]
        public void TestBuilder_greater_than_max()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("").WithRadius(40001);
            var request = builder.Build();
            Assert.IsNull(request.Radius);
        }

        [TestMethod]
        public void TestBuilder_point()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("").WithPoint(58.654321, 91.456789);
            var request = builder.Build();
            Assert.AreEqual("58.654321,91.456789", request.Point);
        }

        [TestMethod]
        public void TestBuilder_top_left()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("").WithTopLeftBound(1.9832, 2.91973);
            var request = builder.Build();
            Assert.AreEqual("1.9832,2.91973", request.TopLeftBound);
        }

        [TestMethod]
        public void TestBuilder_bottom_right()
        {
            var builder = SearchRequestBuilder.CreateRequestWithKey("").WithBottomRightBound(9.1568, 18.546872);
            var request = builder.Build();
            Assert.AreEqual("9.1568,18.546872", request.BottomRightBound);
        }
    }
}
