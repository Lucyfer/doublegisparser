﻿using DoubleGisParser.Builders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.Builders
{
    [TestClass]
    public class ProfileRequestBuilderTest
    {
        [TestMethod]
        public void TestBuilder_empty()
        {
            var builder = ProfileRequestBuilder.CreateRequestWithKey("someKey");
            var request = builder.Build();
            Assert.AreEqual("someKey", request.Key);
            Assert.IsNull(request.Id);
            Assert.IsNull(request.Hash);
        }

        [TestMethod]
        public void TestBuilder_all()
        {
            var builder = ProfileRequestBuilder.CreateRequestWithKey("someKey")
                                               .WithFirmId("123456")
                                               .WithAffiliateHash("qwerty");
            var request = builder.Build();
            Assert.AreEqual("someKey", request.Key);
            Assert.AreEqual("123456", request.Id);
            Assert.AreEqual("qwerty", request.Hash);
        }

        [TestMethod]
        public void TestBuilder_firm_id()
        {
            var builder = ProfileRequestBuilder.CreateRequestWithKey("someKey")
                                               .WithFirmId("123456");
            var request = builder.Build();
            Assert.AreEqual("someKey", request.Key);
            Assert.AreEqual("123456", request.Id);
            Assert.IsNull(request.Hash);
        }

        [TestMethod]
        public void TestBuilder_affiliate_hash()
        {
            var builder = ProfileRequestBuilder.CreateRequestWithKey("someKey")
                                               .WithAffiliateHash("qwerty");
            var request = builder.Build();
            Assert.AreEqual("someKey", request.Key);
            Assert.IsNull(request.Id);
            Assert.AreEqual("qwerty", request.Hash);
        }
    }
}
