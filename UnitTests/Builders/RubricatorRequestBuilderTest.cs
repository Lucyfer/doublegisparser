﻿using DoubleGisParser.Builders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.Builders
{
    [TestClass]
    public class RubricatorRequestBuilderTest
    {
        [TestMethod]
        public void TestBuilder_empty()
        {
            var builder = RubricatorRequestBuilder.CreateRequestWithKey("someKey");
            var request = builder.Build();
            Assert.AreEqual("1.3", request.Version);
            Assert.AreEqual("someKey", request.Key);
            Assert.IsNull(request.Where);
            Assert.IsNull(request.Id);
            Assert.IsNull(request.ParentId);
            Assert.IsNull(request.ShowChildren);
            Assert.IsNull(request.Sort);
        }

        [TestMethod]
        public void TestBuilder_full()
        {
            var builder = RubricatorRequestBuilder.CreateRequestWithKey("someKey")
                                                  .ForCity("city")
                                                  .WithRubricId(10)
                                                  .WithParentRubricId(20)
                                                  .IncludeChildrenRubrics(true)
                                                  .SetSort(RubricatorRequestBuilder.Sort.Alphabetically);
            var request = builder.Build();
            Assert.AreEqual("1.3", request.Version);
            Assert.AreEqual("someKey", request.Key);
            Assert.AreEqual("city", request.Where);
            Assert.AreEqual(10, request.Id);
            Assert.AreEqual(20, request.ParentId);
            Assert.AreEqual(1, request.ShowChildren);
            Assert.AreEqual("name", request.Sort);
        }

        [TestMethod]
        public void TestBuilder_sort_by_popularity()
        {
            var builder = RubricatorRequestBuilder.CreateRequestWithKey("someKey")
                                                  .SetSort(RubricatorRequestBuilder.Sort.Popularity);
            var requst = builder.Build();
            Assert.AreEqual("popularity", requst.Sort);
        }

        [TestMethod]
        public void TestBuilder_sort_alphabetically()
        {
            var builder = RubricatorRequestBuilder.CreateRequestWithKey("someKey")
                                                  .SetSort(RubricatorRequestBuilder.Sort.Alphabetically);
            var requst = builder.Build();
            Assert.AreEqual("name", requst.Sort);
        }

        [TestMethod]
        public void TestBuilder_show_children_is_true()
        {
            var builder = RubricatorRequestBuilder.CreateRequestWithKey("someKey")
                                                  .IncludeChildrenRubrics(true);
            var requst = builder.Build();
            Assert.AreEqual(1, requst.ShowChildren);
        }

        [TestMethod]
        public void TestBuilder_show_children_is_false()
        {
            var builder = RubricatorRequestBuilder.CreateRequestWithKey("someKey")
                                                  .IncludeChildrenRubrics(false);
            var requst = builder.Build();
            Assert.AreEqual(0, requst.ShowChildren);
        }
    }
}
