properties {
    $testLocation = ".\bin\Release"
}

Task Default -depends Tests

Task Tests -depends BuildTests {
    Chdir $testLocation
    Exec { & $MSTest $testContainer }
}

Task BuildTests {
    Exec { msbuild "$testProject.csproj" }
}
