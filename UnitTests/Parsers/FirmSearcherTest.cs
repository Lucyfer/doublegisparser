﻿using System.IO;
using System.Linq;
using DoubleGisParser.Converters.Impl;
using DoubleGisParser.Models.Requests;
using DoubleGisParser.Parsers;
using DoubleGisParser.Parsers.Impl;
using DoubleGisParser.Transports;
using DoubleGisParser.Transports.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests.Parsers
{
    [TestClass]
    public class FirmSearcherTest
    {
        private IFirmSearcher _searcher;

        [TestMethod]
        [DeploymentItem(@"TestData\FirmSearch.json", "TestData")]
        public void TestExecute_with_mock()
        {
            var mock = new Mock<IHttpDownloader>();
            mock.Setup(d => d.Download(It.IsAny<string>())).Returns(TestResponse());
            _searcher = new FirmSearcher(mock.Object, new QueryParamsConverter());

            var result = _searcher.Execute(new SearchRequestModel("ruaonj9408", "", ""));
            Assert.IsFalse(result.IsError);

            var response = result.Normal;
            Assert.AreEqual("200", response.ResponseCode);
            Assert.AreEqual("музей", response.What);
            Assert.AreEqual("Москва", response.Where);
            Assert.AreEqual(2, response.Total);
            Assert.AreEqual(2, response.Result.Count());

            var first = response.Result.First();
            Assert.AreEqual("4504127908471392", first.Id);
            Assert.AreEqual("Царицыно, государственный музей-заповедник", first.Name);
            Assert.AreEqual(1, first.FirmGroup.Count);
        }

        [TestMethod]
        [DeploymentItem(@"TestData\FirmSearch2.json", "TestData")]
        public void TestExecute_with_mock2()
        {
            var mock = new Mock<IHttpDownloader>();
            mock.Setup(d => d.Download(It.IsAny<string>())).Returns(TestResponse2());
            _searcher = new FirmSearcher(mock.Object, new QueryParamsConverter());

            var result = _searcher.Execute(new SearchRequestModel("ruaonj9408", "", ""));
            Assert.IsFalse(result.IsError);

            var response = result.Normal;
            Assert.AreEqual("200", response.ResponseCode);
            Assert.AreEqual("музей", response.What);
            Assert.AreEqual("Барнаул", response.Where);
            Assert.AreEqual(3, response.Total);
            Assert.AreEqual(3, response.Result.Count());

            var first = response.Result.First();
            Assert.AreEqual("563478235079640", first.Id);
            Assert.AreEqual("Планета удовольствий, музей", first.Name);
            Assert.AreEqual(1, first.FirmGroup.Count);
        }

        [TestMethod]
        [Ignore]
        public void TestExecute_real_api_invocation()
        {
            _searcher = new FirmSearcher(new HttpDownloader(), new QueryParamsConverter());

            var result = _searcher.Execute(new SearchRequestModel("ruaonj9408", "Бары", "Барнаул, Ленина, 8"));
            Assert.IsFalse(result.IsError);

            var response = result.Normal;
            Assert.AreEqual("200", response.ResponseCode);
            Assert.AreEqual("Бары", response.What);
            Assert.AreEqual("Барнаул, Ленина проспект, 8 / Пушкина, 35", response.Where);
            Assert.IsNotNull(response.Result);
        }

        #region Test Data

        private static string TestResponse()
        {
            return File.ReadAllText(@"TestData\FirmSearch.json");
        }

        private static string TestResponse2()
        {
            return File.ReadAllText(@"TestData\FirmSearch2.json");
        }

        #endregion
    }
}
