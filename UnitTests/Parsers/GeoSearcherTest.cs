﻿using System.IO;
using System.Linq;
using DoubleGisParser.Converters.Impl;
using DoubleGisParser.Models.Requests;
using DoubleGisParser.Parsers;
using DoubleGisParser.Parsers.Impl;
using DoubleGisParser.Transports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests.Parsers
{
    [TestClass]
    public class GeoSearcherTest
    {
        private IGeoSearcher _searcher;

        [TestMethod]
        [DeploymentItem(@"TestData\GeoSearch.json", "TestData")]
        public void TestExecute_with_mock()
        {
            var mock = new Mock<IHttpDownloader>();
            mock.Setup(d => d.Download(It.IsAny<string>())).Returns(TestResponse());
            _searcher = new GeoSearcher(mock.Object, new QueryParamsConverter());

            var resultType = _searcher.Execute(new GeoRequestModel("ruaonj9408", ""));
            Assert.IsFalse(resultType.IsError);

            var response = resultType.Normal;
            Assert.AreEqual("200", response.ResponseCode);
            Assert.AreEqual(1, response.Total);

            var results = response.Result.Single();
            Assert.AreEqual("563572723679253", results.Id);
            Assert.AreEqual(4, results.ProjectId);
            Assert.AreEqual("city", results.Type);
            Assert.AreEqual("Барнаул", results.Name);
            Assert.AreEqual("Барнаул", results.ShortName);
            Assert.AreEqual("POINT(83.7769831204128 53.3427484153949)", results.Centroid);
            Assert.AreEqual(0, results.Dist);

            var attributes = results.Attributes;
            Assert.AreEqual("г.", attributes.Abbreviation);
            Assert.AreEqual("Город", attributes.Type);
            Assert.AreEqual(509, attributes.FirmCount);
            Assert.AreEqual(3, attributes.FirmCountByRubrics.Count);

            var counter = attributes.FirmCountByRubrics.First();
            Assert.AreEqual("563070212506358", counter.RubricId);
            Assert.AreEqual("Жилищно-коммунальные услуги", counter.RubricName);
            Assert.AreEqual(18, counter.FirmCount);
        }

        #region Test Data

        private static string TestResponse()
        {
            return File.ReadAllText(@"TestData\GeoSearch.json");
        }

        #endregion
    }
}
