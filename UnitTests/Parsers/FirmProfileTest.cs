﻿using System.IO;
using System.Linq;
using DoubleGisParser.Converters.Impl;
using DoubleGisParser.Models.Requests;
using DoubleGisParser.Parsers;
using DoubleGisParser.Parsers.Impl;
using DoubleGisParser.Transports;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests.Parsers
{
    [TestClass]
    public class FirmProfileTest
    {
        private IFirmProfile _profile;

        [TestMethod]
        [DeploymentItem(@"TestData\FirmProfile.json", "TestData")]
        public void TestExecute_with_mock()
        {
            var mock = new Mock<IHttpDownloader>();
            mock.Setup(d => d.Download(It.IsAny<string>())).Returns(TestResponse());
            _profile = new FirmProfile(mock.Object, new QueryParamsConverter());

            var result = _profile.Execute(new ProfileRequestModel("ruaonj9408", "", ""));
            Assert.IsFalse(result.IsError);

            var response = result.Normal;
            Assert.AreEqual("200", response.ResponseCode);
            Assert.AreEqual("4504127911171856", response.Id);
            Assert.AreEqual("Му-Му, сеть кафе быстрого питания", response.Name);
            Assert.AreEqual(31, response.FirmGroup.FilialsCount);
            Assert.AreEqual("Москва", response.CityName);
            Assert.AreEqual(2, response.Contacts.Count);

            var contact = response.Contacts.First();
            Assert.AreEqual("", contact.Name);
            Assert.AreEqual(2, contact.Contacts.Count);
            Assert.AreEqual("(499) 261-36-76", contact.Contacts.First().Value);

            var schedule = response.Schedule;
            Assert.IsNotNull(schedule.Mon);
            Assert.IsNotNull(schedule.Thu);
            Assert.IsNotNull(schedule.Wed);
            Assert.IsNotNull(schedule.Thu);
            Assert.IsNotNull(schedule.Fri);
            Assert.IsNotNull(schedule.Sat);
            Assert.IsNotNull(schedule.Sun);
            Assert.IsNull(schedule.Comment);

            var rubrics = response.Rubrics;
            Assert.AreEqual("Кафе / рестораны быстрого питания", rubrics.Single());
        }

        #region Test Data

        private static string TestResponse()
        {
            return File.ReadAllText(@"TestData\FirmProfile.json");
        }

        #endregion
    }
}
