﻿using System.Linq;
using DoubleGisParser.Converters.Impl;
using DoubleGisParser.Models.Requests;
using DoubleGisParser.Parsers;
using DoubleGisParser.Parsers.Impl;
using DoubleGisParser.Transports;
using DoubleGisParser.Transports.Impl;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTests.Parsers
{
    [TestClass]
    public class RubricatorTest
    {
        private IRubricator _rubricator;

        [TestInitialize]
        public void SetUp()
        {
        }

        [TestMethod]
        public void TestExecute_with_mock()
        {
            var mock = new Mock<IHttpDownloader>();
            mock.Setup(d => d.Download(It.IsAny<string>())).Returns(TestResponse);
            _rubricator = new Rubricator(mock.Object, new QueryParamsConverter());

            var result = _rubricator.Execute(new RubricatorRequestModel("ruaonj9408", "Новосибирск"));
            Assert.IsFalse(result.IsError);

            var response = result.Normal;
            Assert.AreEqual("200", response.ResponseCode);
            Assert.AreEqual("39247592", response.ParentId);
            Assert.AreEqual(27, response.Total);
            Assert.AreEqual(2, response.Result.Count());

            var first = response.Result.First();
            Assert.AreEqual("4503719886460203", first.Id);
            Assert.AreEqual("Аварийные / справочные / экстренные службы", first.Name);
            Assert.AreEqual("avarijjnye_spravochnye_ehkstrennye_sluzhby", first.Alias);
            Assert.IsNull(first.Children);
        }

        [TestMethod]
        [Ignore]
        public void TestExecute_real_api_invocation()
        {
            _rubricator = new Rubricator(new HttpDownloader(), new QueryParamsConverter());

            var result = _rubricator.Execute(new RubricatorRequestModel("ruaonj9408", "Новосибирск"));

            Assert.IsFalse(result.IsError);

            var response = result.Normal;
            Assert.AreEqual("200", response.ResponseCode);
            Assert.AreEqual(27, response.Total);
        }

        #region Stub Data

        private const string TestResponse = @"{
            api_version: ""1.3"",
            response_code: ""200"",
            parent_id: ""39247592"",
            total: 27,
            result: [
                {
                    id: ""4503719886460203"",
                    name: ""Аварийные / справочные / экстренные службы"",
                    alias: ""avarijjnye_spravochnye_ehkstrennye_sluzhby"",
                    parent_id: """"
                },
                {
                    id: ""4503719886497687"",
                    name: ""Автосервис / Автотовары"",
                    alias: ""avtoservis_avtotovary"",
                    parent_id: """"
                }
            ]
        }";

        #endregion
    }
}
