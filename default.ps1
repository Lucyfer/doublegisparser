properties {
    $psake.use_exit_on_error = $true

    $mainProject = 'DoubleGisParser'
    $testProject = 'UnitTests'
    $packageProject = 'self-package'
    $packageRepo = 'D:\nuget-repo'

    $testContainer = "/testcontainer:$testProject.dll"
    $MSTest = "$env:VS100COMNTOOLS\..\IDE\MSTest.exe"
}

Task Default -depends Package

Task Package -depends Tests {
    Invoke-psake .\$packageProject\default.ps1
}

Task Tests -depends Build {
    Invoke-psake .\$testProject\default.ps1
}

Task Build {
    Invoke-psake .\$mainProject\default.ps1
}
