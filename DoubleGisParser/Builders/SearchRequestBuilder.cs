﻿using System.Collections.Generic;
using System.Globalization;
using DoubleGisParser.Models.Requests;

namespace DoubleGisParser.Builders
{
    public class SearchRequestBuilder
    {
        #region Required

        private string _key;
        private string _what;
        private string _where;

        #endregion

        #region Optional

        private string _bottomRight;
        private string _filters;
        private int? _page;
        private int? _pageSize;
        private string _point;
        private int? _radius;
        private string _sort;
        private string _topLeft;

        #endregion

        #region Enums

        public enum Days
        {
            Mon,
            Tue,
            Wed,
            Thu,
            Fri,
            Sat,
            Sun
        }

        public enum Sort
        {
            Relevance,
            Rating,
            Name,
            Distance
        }

        private static readonly Dictionary<Sort, string> SortToString = new Dictionary<Sort, string>
        {
            { Sort.Relevance, "relevance" },
            { Sort.Rating, "rating" },
            { Sort.Name, "name" },
            { Sort.Distance, "distance" }
        };

        private static readonly Dictionary<Days, string> DaysToString = new Dictionary<Days, string>
        {
            { Days.Mon, "mon" },
            { Days.Tue, "tue" },
            { Days.Wed, "wed" },
            { Days.Thu, "thu" },
            { Days.Fri, "fri" },
            { Days.Sat, "sat" },
            { Days.Sun, "sun" }
        };

        #endregion

        private SearchRequestBuilder()
        {
        }

        public static SearchRequestBuilder CreateRequestWithKey(string key)
        {
            return new SearchRequestBuilder { _key = key };
        }

        public SearchRequestBuilder SearchFor(string what)
        {
            _what = what;
            return this;
        }

        public SearchRequestBuilder WithWhere(string @where)
        {
            _where = @where;
            return this;
        }

        public SearchRequestBuilder SetWorktimeFilter(Days day, int hour, int minute)
        {
            if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 59)
            {
                _filters = string.Format("filters[worktime]={0},{1:00}:{2:00}", DaysToString[day], hour, minute);
            }
            else
            {
                _filters = null;
            }
            return this;
        }

        public SearchRequestBuilder SetWorktimeFilter(Days day)
        {
            _filters = string.Format("filters[worktime]={0},alltime", DaysToString[day]);
            return this;
        }

        public SearchRequestBuilder SetPage(int page)
        {
            _page = page;
            return this;
        }

        public SearchRequestBuilder SetPageSize(int pageSize)
        {
            _pageSize = pageSize;
            return this;
        }

        public SearchRequestBuilder SetSorting(Sort sort)
        {
            _sort = SortToString[sort];
            return this;
        }

        public SearchRequestBuilder WithRadius(int radius)
        {
            _radius = radius >= 1 && radius <= 40000 ? (int?) radius : null;
            return this;
        }

        public SearchRequestBuilder WithPoint(double lng, double lat)
        {
            _point = LongLatToString(lng, lat);
            return this;
        }

        public SearchRequestBuilder WithTopLeftBound(double lng, double lat)
        {
            _topLeft = LongLatToString(lng, lat);
            return this;
        }

        public SearchRequestBuilder WithBottomRightBound(double lng, double lat)
        {
            _bottomRight = LongLatToString(lng, lat);
            return this;
        }

        public SearchRequestModel Build()
        {
            return new SearchRequestModel(_key, _what, _where)
            {
                Filters = _filters,
                Page = _page,
                PageSize = _pageSize,
                Sort = _sort,
                Radius = _radius,
                Point = _point,
                BottomRightBound = _bottomRight,
                TopLeftBound = _topLeft
            };
        }

        private static string LongLatToString(double lng, double lat)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:R},{1:R}", lng, lat);
        }
    }
}
