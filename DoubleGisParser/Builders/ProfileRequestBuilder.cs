﻿using DoubleGisParser.Models.Requests;

namespace DoubleGisParser.Builders
{
    public class ProfileRequestBuilder
    {
        #region Required

        private string _hash;
        private string _id;
        private string _key;

        #endregion

        private ProfileRequestBuilder()
        {
        }

        public static ProfileRequestBuilder CreateRequestWithKey(string key)
        {
            return new ProfileRequestBuilder { _key = key };
        }

        public ProfileRequestBuilder WithFirmId(string id)
        {
            _id = id;
            return this;
        }

        public ProfileRequestBuilder WithAffiliateHash(string hash)
        {
            _hash = hash;
            return this;
        }

        public ProfileRequestModel Build()
        {
            return new ProfileRequestModel(_key, _id, _hash);
        }
    }
}
