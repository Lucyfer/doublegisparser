﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DoubleGisParser.Models.Requests;

namespace DoubleGisParser.Builders
{
    public class GeoRequestBuilder
    {
        #region Required

        private string _key;
        private string _query;

        #endregion

        #region Optional

        private string _bottomRight;
        private string _format;
        private int? _limit;
        private int? _project;
        private int? _radius;
        private string _topLeft;
        private string _types;

        #endregion

        #region Enums

        public enum Format
        {
            Full,
            Short
        }

        public enum Types
        {
            Project,
            District,
            House,
            City,
            Settlement,
            Station,
            StationPlatform,
            Street,
            LivingArea,
            Place,
            Sight,
            Crossroad,
            Metro
        }

        private static readonly Dictionary<Format, string> FormatToString = new Dictionary<Format, string>
        {
            { Format.Full, "full" },
            { Format.Short, "short" }
        };

        private static readonly Dictionary<Types, string> TypeToString = new Dictionary<Types, string>
        {
            { Types.Project, "project" },
            { Types.District, "district" },
            { Types.House, "house" },
            { Types.City, "city" },
            { Types.Settlement, "settlement" },
            { Types.Station, "station" },
            { Types.StationPlatform, "station_platform" },
            { Types.Street, "street" },
            { Types.LivingArea, "living_area" },
            { Types.Place, "place" },
            { Types.Sight, "sight" },
            { Types.Crossroad, "crossroad" },
            { Types.Metro, "metro" }
        };

        #endregion

        private GeoRequestBuilder()
        {
        }

        public static GeoRequestBuilder CreateRequestWithKey(string key)
        {
            return new GeoRequestBuilder { _key = key };
        }

        public GeoRequestBuilder WithPoint(double lng, double lat)
        {
            _query = LongLatToString(lng, lat);
            return this;
        }

        public GeoRequestBuilder WithCustomQuery(string query)
        {
            _query = query;
            return this;
        }

        public GeoRequestBuilder WithRadius(int radius)
        {
            _radius = radius >= 0 && radius <= 250 ? (int?) radius : null;
            return this;
        }

        public GeoRequestBuilder WithProject(int project)
        {
            _project = project;
            return this;
        }

        public GeoRequestBuilder WithTopLeftBound(double lng, double lat)
        {
            _topLeft = LongLatToString(lng, lat);
            return this;
        }

        public GeoRequestBuilder WithBottomRightBound(double lng, double lat)
        {
            _bottomRight = LongLatToString(lng, lat);
            return this;
        }

        public GeoRequestBuilder WithTypes(params Types[] types)
        {
            _types = string.Join(",", types.Select(type => TypeToString[type]));
            return this;
        }

        public GeoRequestBuilder SetFormat(Format format)
        {
            _format = FormatToString[format];
            return this;
        }

        public GeoRequestBuilder SetLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public GeoRequestModel Build()
        {
            return new GeoRequestModel(_key, _query)
            {
                Project = _project,
                Radius = _radius,
                Limit = _limit,
                Types = _types,
                Format = _format,
                TopLeftBound = _topLeft,
                BottomRightBound = _bottomRight
            };
        }

        private static string LongLatToString(double lng, double lat)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0:R},{1:R}", lng, lat);
        }
    }
}
