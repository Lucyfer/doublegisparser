﻿using System.Collections.Generic;
using DoubleGisParser.Models.Requests;

namespace DoubleGisParser.Builders
{
    public class RubricatorRequestBuilder
    {
        #region Required

        private string _key;
        private string _where;

        #endregion

        #region Optional

        private int? _id;
        private int? _parentId;
        private int? _showChildren;
        private string _sort;

        #endregion

        #region Enums

        public enum Sort
        {
            Alphabetically,
            Popularity
        }

        private static readonly Dictionary<Sort, string> SortToString = new Dictionary<Sort, string>
        {
            { Sort.Alphabetically, "name" },
            { Sort.Popularity, "popularity" }
        };

        #endregion

        private RubricatorRequestBuilder()
        {
        }

        public static RubricatorRequestBuilder CreateRequestWithKey(string key)
        {
            return new RubricatorRequestBuilder { _key = key };
        }

        public RubricatorRequestBuilder ForCity(string city)
        {
            _where = city;
            return this;
        }

        public RubricatorRequestBuilder WithRubricId(int id)
        {
            _id = id;
            return this;
        }

        public RubricatorRequestBuilder WithParentRubricId(int parentId)
        {
            _parentId = parentId;
            return this;
        }

        public RubricatorRequestBuilder IncludeChildrenRubrics(bool include)
        {
            _showChildren = include ? 1 : 0;
            return this;
        }

        public RubricatorRequestBuilder SetSort(Sort sort)
        {
            _sort = SortToString[sort];
            return this;
        }

        public RubricatorRequestModel Build()
        {
            return new RubricatorRequestModel(_key, _where)
            {
                Id = _id,
                ParentId = _parentId,
                ShowChildren = _showChildren,
                Sort = _sort
            };
        }
    }
}
