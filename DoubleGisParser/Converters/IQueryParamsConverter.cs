namespace DoubleGisParser.Converters
{
    public interface IQueryParamsConverter
    {
        string ToQueryParams(object obj);
    }
}