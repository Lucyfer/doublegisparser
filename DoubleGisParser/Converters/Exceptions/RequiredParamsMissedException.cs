﻿using System;

namespace DoubleGisParser.Converters.Exceptions
{
    public class RequiredParamsMissedException : Exception
    {
        public RequiredParamsMissedException(string message) : base(message)
        {
        }

        public RequiredParamsMissedException()
        {
        }
    }
}
