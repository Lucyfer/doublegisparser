﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;
using DoubleGisParser.Converters.Exceptions;

namespace DoubleGisParser.Converters.Impl
{
    /// <summary>
    /// Convert POCO to query params
    /// </summary>
    public class QueryParamsConverter : IQueryParamsConverter
    {
        /// <summary>
        /// Convert the public fields and properties except the marked IgnoreDataMember attribute.
        /// </summary>
        /// <param name="obj">Convertable object</param>
        /// <returns></returns>
        public string ToQueryParams(object obj)
        {
            var lst = new List<string>();
            var type = obj.GetType();

            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            ProcessMembers(lst, properties, obj);

            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            ProcessMembers(lst, fields, obj);

            return string.Join("&", lst);
        }

        private static void ProcessMembers(ICollection<string> result, IList<MemberInfo> array, object src)
        {
            foreach (var info in array)
            {
                var name = GetMemberName(info);
                if (string.IsNullOrEmpty(name)) continue;

                var required = IsRequiredValue(info);

                var value = GetMemberValue(info, src);
                if (value == null)
                {
                    if (required)
                    {
                        throw new RequiredParamsMissedException(string.Format("Required value for '{0}'", name));
                    }
                    continue;
                }

                AppendQueryParamAndValue(result, name, value);
            }
        }

        private static string GetMemberName(MemberInfo info)
        {
            var name = info.Name;
            var attrs = info.GetCustomAttributes(true);
            foreach (var attr in attrs)
            {
                if (attr is DataMemberAttribute)
                {
                    var dataMember = attr as DataMemberAttribute;
                    if (!string.IsNullOrEmpty(dataMember.Name))
                    {
                        name = dataMember.Name;
                    }
                }
                else if (attr is IgnoreDataMemberAttribute)
                {
                    return null;
                }
            }
            return name;
        }

        private static bool IsRequiredValue(MemberInfo info)
        {
            var attrs = info.GetCustomAttributes(true);
            return attrs.OfType<DataMemberAttribute>().Select(m => m.IsRequired).FirstOrDefault();
        }

        private static object GetMemberValue(object info, object src)
        {
            if (info is FieldInfo)
            {
                return (info as FieldInfo).GetValue(src);
            }
            if (info is PropertyInfo)
            {
                return (info as PropertyInfo).GetValue(src, null);
            }
            return null;
        }

        private static void AppendQueryParamAndValue(ICollection<string> result, string paramName, object paramValue)
        {
            var stringValue = string.Format(CultureInfo.InvariantCulture, "{0}", paramValue);
            var encodedValue = HttpUtility.UrlEncode(stringValue);
            result.Add(string.Format("{0}={1}", paramName, encodedValue));
        }
    }
}
