using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Responses
{
    [DataContract]
    public class ResponseErrorModel
    {
        [DataMember(Name = "response_code")]
        public string ResponseCode { get; set; }

        [DataMember(Name = "error_code")]
        public string ErrorCode { get; set; }

        [DataMember(Name = "error_message")]
        public string ErrorMessage { get; set; }
    }
}
