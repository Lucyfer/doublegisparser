﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Responses
{
    [DataContract]
    public class RubricatorResponseModel
    {
        [DataMember(Name = "response_code")]
        public string ResponseCode { get; set; }

        [DataMember(Name = "parent_id")]
        public string ParentId { get; set; }

        [DataMember(Name = "total")]
        public int Total { get; set; }

        [DataMember(Name = "result")]
        public IEnumerable<RubricModel> Result { get; set; }

        [DataContract]
        public class ChildModel
        {
            [DataMember(Name = "id")]
            public string Id { get; set; }

            [DataMember(Name = "name")]
            public string Name { get; set; }

            [DataMember(Name = "alias")]
            public string Alias { get; set; }
        }

        [DataContract]
        public class RubricModel
        {
            [DataMember(Name = "id")]
            public string Id { get; set; }

            [DataMember(Name = "name")]
            public string Name { get; set; }

            [DataMember(Name = "alias")]
            public string Alias { get; set; }

            [DataMember(Name = "parent_id")]
            public string ParentId { get; set; }

            [DataMember(Name = "children", IsRequired = false)]
            public IEnumerable<ChildModel> Children { get; set; }
        }
    }
}
