﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Responses
{
    [DataContract]
    public class GeoResponseModel
    {
        [DataMember(Name = "response_code")]
        public string ResponseCode;

        [DataMember(Name = "result")]
        public IList<GeoObjectModel> Result;

        [DataMember(Name = "total")]
        public int Total;

        [DataContract]
        public class AttributesModel
        {
            [DataMember(Name = "abbreviation")]
            public string Abbreviation;

            [DataMember(Name = "buildingname")]
            public string BuildingName;

            [DataMember(Name = "city")]
            public string City;

            [DataMember(Name = "district")]
            public string District;

            [DataMember(Name = "elevation")]
            public int Elevation;

            [DataMember(Name = "firmcount")]
            public int FirmCount;

            [DataMember(Name = "firmcount_by_rubrics")]
            public IList<RubricCountModel> FirmCountByRubrics;

            [DataMember(Name = "index")]
            public string Index;

            [DataMember(Name = "info")]
            public string Info;

            [DataMember(Name = "mapclass")]
            public string MapClass;

            [DataMember(Name = "number")]
            public string Number;

            [DataMember(Name = "number2")]
            public string Number2;

            [DataMember(Name = "purpose")]
            public string Purpose;

            [DataMember(Name = "street")]
            public string Street;

            [DataMember(Name = "street2")]
            public string Street2;

            [DataMember(Name = "synonym")]
            public string Synonym;

            [DataMember(Name = "type")]
            public string Type;
        }

        [DataContract]
        public class GeoObjectModel
        {
            [DataMember(Name = "attributes")]
            public AttributesModel Attributes;

            [DataMember(Name = "centroid")]
            public string Centroid;

            [DataMember(Name = "dist")]
            public int Dist;

            [DataMember(Name = "id")]
            public string Id;

            [DataMember(Name = "name")]
            public string Name;

            [DataMember(Name = "project_id")]
            public int ProjectId;

            [DataMember(Name = "selection")]
            public string Selection;

            [DataMember(Name = "short_name")]
            public string ShortName;

            [DataMember(Name = "type")]
            public string Type;
        }

        [DataContract]
        public class RubricCountModel
        {
            [DataMember(Name = "firmcount")]
            public int FirmCount;

            [DataMember(Name = "rubric_id")]
            public string RubricId;

            [DataMember(Name = "rubric_name")]
            public string RubricName;
        }
    }
}
