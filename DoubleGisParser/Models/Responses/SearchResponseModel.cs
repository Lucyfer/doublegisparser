using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Responses
{
    [DataContract]
    public class SearchResponseModel
    {
        [DataMember(Name = "response_code")]
        public string ResponseCode;

        [DataMember(Name = "what")]
        public string What;

        [DataMember(Name = "where")]
        public string Where;

        [DataMember(Name = "total")]
        public int Total;

        [DataMember(Name = "result")]
        public IList<AffiliateModel> Result;

        [DataContract]
        public class AffiliateModel
        {
            [DataMember(Name = "id")]
            public string Id;

            [DataMember(Name = "lon")]
            public string Lon;

            [DataMember(Name = "lat")]
            public string Lat;

            [DataMember(Name = "name")]
            public string Name;

            [DataMember(Name = "firm_group")]
            public FirmGroup FirmGroup;

            [DataMember(Name = "hash")]
            public string Hash;

            [DataMember(Name = "city_name")]
            public string CityName;

            [DataMember(Name = "address")]
            public string Address;

            [DataMember(Name = "rubrics")]
            public IList<string> Rubrics;
        }

        [DataContract]
        public class FirmGroup
        {
            [DataMember(Name = "id")]
            public string Id;

            [DataMember(Name = "count")]
            public int Count;
        }
    }
}