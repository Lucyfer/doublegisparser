﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Responses
{
    [DataContract]
    public class ProfileResponseModel
    {
        [DataMember(Name = "address")]
        public string Address;

        [DataMember(Name = "city_name")]
        public string CityName;

        [DataMember(Name = "contacts")]
        public IList<ContactModel> Contacts;

        [DataMember(Name = "firm_group")]
        public FirmModel FirmGroup;

        [DataMember(Name = "id")]
        public string Id;

        [DataMember(Name = "lat")]
        public string Lat;

        [DataMember(Name = "lon")]
        public string Lon;

        [DataMember(Name = "name")]
        public string Name;

        [DataMember(Name = "payoptions")]
        public IList<string> PayOptions;

        [DataMember(Name = "response_code")]
        public string ResponseCode;

        [DataMember(Name = "rubrics")]
        public IList<string> Rubrics;

        [DataMember(Name = "schedule")]
        public ScheduleModel Schedule;

        [DataContract]
        public class ContactModel
        {
            [DataMember(Name = "contacts")]
            public IList<ContactObjectModel> Contacts;

            [DataMember(Name = "name")]
            public string Name;
        }

        [DataContract]
        public class ContactObjectModel
        {
            [DataMember(Name = "type")]
            public string Type;

            [DataMember(Name = "value")]
            public string Value;
        }

        [DataContract]
        public class FirmModel
        {
            [DataMember(Name = "count")]
            public int FilialsCount;

            [DataMember(Name = "id")]
            public string Id;
        }

        [DataContract]
        public class HoursObjectModel
        {
            [DataMember(Name = "working_hours-0")]
            public WorkingHoursModel WorkingHours0;

            [DataMember(Name = "working_hours-1")]
            public WorkingHoursModel WorkingHours1;

            [DataMember(Name = "working_hours-2")]
            public WorkingHoursModel WorkingHours2;
        }

        [DataContract]
        public class ScheduleModel
        {
            [DataMember(Name = "comment")]
            public string Comment;

            [DataMember(Name = "Fri")]
            public HoursObjectModel Fri;

            [DataMember(Name = "Mon")]
            public HoursObjectModel Mon;

            [DataMember(Name = "Sat")]
            public HoursObjectModel Sat;

            [DataMember(Name = "Sun")]
            public HoursObjectModel Sun;

            [DataMember(Name = "Thu")]
            public HoursObjectModel Thu;

            [DataMember(Name = "Tue")]
            public HoursObjectModel Tue;

            [DataMember(Name = "Wed")]
            public HoursObjectModel Wed;
        }

        [DataContract]
        public class WorkingHoursModel
        {
            [DataMember(Name = "from")]
            public string From;

            [DataMember(Name = "to")]
            public string To;
        }
    }
}
