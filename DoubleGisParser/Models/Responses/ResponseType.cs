﻿namespace DoubleGisParser.Models.Responses
{
    public sealed class ResponseType<TNormal, TError>
        where TNormal : class
        where TError : class
    {
        private readonly TError _error;
        private readonly TNormal _normal;

        private ResponseType(TNormal normal, TError error)
        {
            _normal = normal;
            _error = error;
        }

        public bool IsError
        {
            get { return _normal == null; }
        }

        public TError Error
        {
            get { return _error; }
        }

        public TNormal Normal
        {
            get { return _normal; }
        }

        public static ResponseType<TNormal, TError> CreateNormal(TNormal normal)
        {
            return new ResponseType<TNormal, TError>(normal, null);
        }

        public static ResponseType<TNormal, TError> CreateError(TError error)
        {
            return new ResponseType<TNormal, TError>(null, error);
        }
    }
}
