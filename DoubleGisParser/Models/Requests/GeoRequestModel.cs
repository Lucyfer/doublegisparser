﻿using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Requests
{
    [DataContract]
    public class GeoRequestModel
    {
        [DataMember(Name = "version", IsRequired = true)]
        public readonly string Version;

        #region Required

        [DataMember(Name = "key", IsRequired = true)]
        public string Key;

        [DataMember(Name = "q", IsRequired = true)]
        public string Query;

        #endregion

        #region Optional

        [DataMember(Name = "bound[point2]")]
        public string BottomRightBound;

        [DataMember(Name = "format")]
        public string Format;

        [DataMember(Name = "limit")]
        public int? Limit;

        [DataMember(Name = "project")]
        public int? Project;

        [DataMember(Name = "radius")]
        public int? Radius;

        [DataMember(Name = "bound[point1]")]
        public string TopLeftBound;

        [DataMember(Name = "types")]
        public string Types;

        #endregion

        public GeoRequestModel()
        {
            Version = "1.3";
        }

        public GeoRequestModel(string key, string query) : this()
        {
            Key = key;
            Query = query;
        }
    }
}
