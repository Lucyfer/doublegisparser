using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Requests
{
    [DataContract]
    public class SearchRequestModel
    {
        [DataMember(Name = "version", IsRequired = true)]
        public readonly string Version;

        #region Required

        [DataMember(Name = "key", IsRequired = true)]
        public string Key;

        [DataMember(Name = "what", IsRequired = true)]
        public string What;

        [DataMember(Name = "where", IsRequired = true)]
        public string Where;

        #endregion

        #region Optional

        [DataMember(Name = "bound[point2]")]
        public string BottomRightBound;

        [DataMember(Name = "filters")]
        public string Filters;

        [DataMember(Name = "page")]
        public int? Page;

        [DataMember(Name = "pagesize")]
        public int? PageSize;

        [DataMember(Name = "point")]
        public string Point;

        [DataMember(Name = "radius")]
        public int? Radius;

        [DataMember(Name = "sort")]
        public string Sort;

        [DataMember(Name = "bound[point1]")]
        public string TopLeftBound;

        #endregion

        public SearchRequestModel()
        {
            Version = "1.3";
        }

        public SearchRequestModel(string key, string what, string @where) : this()
        {
            Key = key;
            What = what;
            Where = @where;
        }
    }
}
