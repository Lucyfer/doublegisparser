﻿using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Requests
{
    public class RubricatorRequestModel
    {
        [DataMember(Name = "version", IsRequired = true)]
        public readonly string Version;

        #region Required

        [DataMember(Name = "key", IsRequired = true)]
        public string Key;

        [DataMember(Name = "where", IsRequired = true)]
        public string Where;

        #endregion

        #region Optional

        [DataMember(Name = "id")]
        public int? Id;

        [DataMember(Name = "parent_id")]
        public int? ParentId;

        [DataMember(Name = "show_children")]
        public int? ShowChildren;

        [DataMember(Name = "sort")]
        public string Sort;

        #endregion

        public RubricatorRequestModel()
        {
            Version = "1.3";
        }

        public RubricatorRequestModel(string key, string where) : this()
        {
            Key = key;
            Where = where;
        }
    }
}
