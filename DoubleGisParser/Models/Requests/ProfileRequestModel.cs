﻿using System.Runtime.Serialization;

namespace DoubleGisParser.Models.Requests
{
    [DataContract]
    public class ProfileRequestModel
    {
        [DataMember(Name = "version", IsRequired = true)]
        public readonly string Version;

        #region Required

        [DataMember(Name = "hash", IsRequired = true)]
        public string Hash;

        [DataMember(Name = "id", IsRequired = true)]
        public string Id;

        [DataMember(Name = "key", IsRequired = true)]
        public string Key;

        #endregion

        public ProfileRequestModel()
        {
            Version = "1.3";
        }

        public ProfileRequestModel(string key, string id, string hash) : this()
        {
            Key = key;
            Id = id;
            Hash = hash;
        }
    }
}
