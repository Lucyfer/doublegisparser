﻿using DoubleGisParser.Converters;
using DoubleGisParser.Converters.Impl;
using DoubleGisParser.Parsers;
using DoubleGisParser.Parsers.Impl;
using DoubleGisParser.Transports;
using DoubleGisParser.Transports.Impl;

namespace DoubleGisParser.Factories.Impl
{
    public class DoubleGisParserFactory : IDoubleGisParserFactory
    {
        #region Parsers

        private readonly IFirmProfile _firmProfile;
        private readonly IFirmSearcher _firmSearcher;
        private readonly IGeoSearcher _geoSearcher;
        private readonly IRubricator _rubricator;

        #endregion

        public DoubleGisParserFactory()
        {
            IHttpDownloader downloader = new HttpDownloader();
            IQueryParamsConverter converter = new QueryParamsConverter();

            _firmProfile = new FirmProfile(downloader, converter);
            _firmSearcher = new FirmSearcher(downloader, converter);
            _geoSearcher = new GeoSearcher(downloader, converter);
            _rubricator = new Rubricator(downloader, converter);
        }

        public IFirmProfile FirmProfile
        {
            get { return _firmProfile; }
        }

        public IFirmSearcher FirmSearcher
        {
            get { return _firmSearcher; }
        }

        public IGeoSearcher GeoSearcher
        {
            get { return _geoSearcher; }
        }

        public IRubricator Rubricator
        {
            get { return _rubricator; }
        }
    }
}
