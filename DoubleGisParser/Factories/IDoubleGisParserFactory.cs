﻿using DoubleGisParser.Parsers;

namespace DoubleGisParser.Factories
{
    public interface IDoubleGisParserFactory
    {
        IFirmProfile FirmProfile { get; }
        IFirmSearcher FirmSearcher { get; }
        IGeoSearcher GeoSearcher { get; }
        IRubricator Rubricator { get; }
    }
}
