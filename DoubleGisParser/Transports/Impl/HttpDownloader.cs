﻿using System;
using System.Net;
using System.Text;

namespace DoubleGisParser.Transports.Impl
{
    public class HttpDownloader : IHttpDownloader
    {
        public string Download(string url)
        {
            return Download(new Uri(url));
        }

        public string Download(Uri uri)
        {
            var client = new WebClient { Encoding = Encoding.UTF8 };
            return client.DownloadString(uri);
        }
    }
}
