namespace DoubleGisParser.Transports
{
    public interface IHttpDownloader
    {
        string Download(string url);
    }
}
