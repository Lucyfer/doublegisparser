﻿using DoubleGisParser.Models.Requests;
using DoubleGisParser.Models.Responses;

namespace DoubleGisParser.Parsers
{
    public interface IFirmSearcher
    {
        ResponseType<SearchResponseModel, ResponseErrorModel> Execute(SearchRequestModel request);
    }
}
