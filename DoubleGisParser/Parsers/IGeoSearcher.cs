﻿using DoubleGisParser.Models.Requests;
using DoubleGisParser.Models.Responses;

namespace DoubleGisParser.Parsers
{
    public interface IGeoSearcher
    {
        ResponseType<GeoResponseModel, ResponseErrorModel> Execute(GeoRequestModel request);
    }
}
