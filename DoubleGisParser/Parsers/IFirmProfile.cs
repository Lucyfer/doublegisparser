﻿using DoubleGisParser.Models.Requests;
using DoubleGisParser.Models.Responses;

namespace DoubleGisParser.Parsers
{
    public interface IFirmProfile
    {
        ResponseType<ProfileResponseModel, ResponseErrorModel> Execute(ProfileRequestModel request);
    }
}
