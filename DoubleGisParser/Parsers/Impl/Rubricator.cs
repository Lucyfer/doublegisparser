﻿using DoubleGisParser.Converters;
using DoubleGisParser.Models.Requests;
using DoubleGisParser.Models.Responses;
using DoubleGisParser.Transports;

namespace DoubleGisParser.Parsers.Impl
{
    public class Rubricator : AbstractParser<RubricatorResponseModel, RubricatorRequestModel>, IRubricator
    {
        private const string CatalogApiRubricator = "http://catalog.api.2gis.ru/rubricator";

        public Rubricator(IHttpDownloader httpDownloader, IQueryParamsConverter converter)
            : base(httpDownloader, converter)
        {
        }

        protected override string GetBaseApiUrl()
        {
            return CatalogApiRubricator;
        }
    }
}
