﻿using System.Collections.Generic;
using DoubleGisParser.Converters;
using DoubleGisParser.Models.Responses;
using DoubleGisParser.Transports;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace DoubleGisParser.Parsers.Impl
{
    public abstract class AbstractParser<TResponse, TRequest>
        where TResponse : class
    {
        private readonly IQueryParamsConverter _converter;
        private readonly IHttpDownloader _httpDownloader;

        protected AbstractParser(IHttpDownloader httpDownloader, IQueryParamsConverter converter)
        {
            _httpDownloader = httpDownloader;
            _converter = converter;
        }

        public virtual ResponseType<TResponse, ResponseErrorModel> Execute(TRequest request)
        {
            var queryParams = _converter.ToQueryParams(request);
            var response = _httpDownloader.Download(PrepareUrl(queryParams));

            ResponseType<TResponse, ResponseErrorModel> result;
            if (IsErrorResponse(response))
            {
                var error = JsonConvert.DeserializeObject<ResponseErrorModel>(response);
                result = ResponseType<TResponse, ResponseErrorModel>.CreateError(error);
            }
            else
            {
                var normal = JsonConvert.DeserializeObject<TResponse>(response);
                result = ResponseType<TResponse, ResponseErrorModel>.CreateNormal(normal);
            }
            return result;
        }

        private static bool IsErrorResponse(string response)
        {
            var schema = new JsonSchema
            {
                Type = JsonSchemaType.Object,
                Properties = new Dictionary<string, JsonSchema>
                {
                    { "api_version", new JsonSchema { Type = JsonSchemaType.String, Required = true } },
                    { "response_code", new JsonSchema { Type = JsonSchemaType.String, Required = true } },
                    { "error_code", new JsonSchema { Type = JsonSchemaType.String, Required = true } },
                    { "error_message", new JsonSchema { Type = JsonSchemaType.String, Required = true } }
                }
            };
            var json = JObject.Parse(response);
            return json.IsValid(schema);
        }

        protected abstract string GetBaseApiUrl();

        protected virtual string PrepareUrl(string queryParams)
        {
            return string.Format("{0}?{1}", GetBaseApiUrl(), queryParams);
        }
    }
}
