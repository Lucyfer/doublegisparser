﻿using DoubleGisParser.Converters;
using DoubleGisParser.Models.Requests;
using DoubleGisParser.Models.Responses;
using DoubleGisParser.Transports;

namespace DoubleGisParser.Parsers.Impl
{
    public class FirmProfile : AbstractParser<ProfileResponseModel, ProfileRequestModel>, IFirmProfile
    {
        private const string CatalogApiProfile = "http://catalog.api.2gis.ru/profile";

        public FirmProfile(IHttpDownloader httpDownloader, IQueryParamsConverter converter)
            : base(httpDownloader, converter)
        {
        }

        protected override string GetBaseApiUrl()
        {
            return CatalogApiProfile;
        }
    }
}
