﻿using DoubleGisParser.Converters;
using DoubleGisParser.Models.Requests;
using DoubleGisParser.Models.Responses;
using DoubleGisParser.Transports;

namespace DoubleGisParser.Parsers.Impl
{
    public class GeoSearcher : AbstractParser<GeoResponseModel, GeoRequestModel>, IGeoSearcher
    {
        private const string CatalogApiGeoSearch = "http://catalog.api.2gis.ru/geo/search";

        public GeoSearcher(IHttpDownloader httpDownloader, IQueryParamsConverter converter)
            : base(httpDownloader, converter)
        {
        }

        protected override string GetBaseApiUrl()
        {
            return CatalogApiGeoSearch;
        }
    }
}
