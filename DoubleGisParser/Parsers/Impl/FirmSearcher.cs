﻿using DoubleGisParser.Converters;
using DoubleGisParser.Models.Requests;
using DoubleGisParser.Models.Responses;
using DoubleGisParser.Transports;

namespace DoubleGisParser.Parsers.Impl
{
    public class FirmSearcher : AbstractParser<SearchResponseModel, SearchRequestModel>, IFirmSearcher
    {
        private const string CatalogApiSearch = "http://catalog.api.2gis.ru/search";

        public FirmSearcher(IHttpDownloader httpDownloader, IQueryParamsConverter converter)
            : base(httpDownloader, converter)
        {
        }

        protected override string GetBaseApiUrl()
        {
            return CatalogApiSearch;
        }
    }
}
