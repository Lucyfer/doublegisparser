﻿using DoubleGisParser.Models.Requests;
using DoubleGisParser.Models.Responses;

namespace DoubleGisParser.Parsers
{
    public interface IRubricator
    {
        ResponseType<RubricatorResponseModel, ResponseErrorModel> Execute(RubricatorRequestModel request);
    }
}
