properties {
    $NuGet = "..\.nuget\NuGet.exe"
}

Task Default -depends Publish

Task Publish -depends Package {
    Move-Item *.nupkg $packageRepo -force
}

Task Package {
    Exec { & $NuGet pack $mainProject.nuspec }
}
